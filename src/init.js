
let pause = time => new Promise(resolve => setTimeout(resolve, time));
async function periodicaly(cb, t) {
  while (true) {
    await cb();
    await pause(t);
  }
}

async function wait_for_un_episode() {
  while (!document.querySelector('#un_episode')) {
    await pause(100);
  }
  return document.querySelector('#un_episode');
}

async function wait_for_title() {
  while (!document.querySelector('h1')) {
    await pause(100);
  }
  return document.querySelector('h1');
}

async function wait_for_jwplayer() {
  while (!jwplayer) {
    await pause(100);
  }
  while (!jwplayer().getPlaylistItem) {
    await pause(100);
  }
  return jwplayer;
}

async function nekosama() {
  const { invoke } = window.__TAURI__.tauri;

  // wait for the iframe to load and goto src
  let un_episode = await wait_for_un_episode();
  let src = un_episode.src;
  let title = (await wait_for_title()).innerText;
  await invoke("set_metadata", { title: title, src: src });
  window.location = src;
}

async function fusevideo() {
  const { invoke } = window.__TAURI__.tauri;

  // wait for jwplayer to be available
  let jwplayer = await wait_for_jwplayer();

  // wait for the video to load
  while (!jwplayer().getPlaylistItem()) {
    await pause(100);
  }

  // send the m3u8 uri to the backend
  let uri = jwplayer().getPlaylistItem().file;
  let title = await invoke("get_metadata", { src: window.location.href });
  await invoke("send_m3u8_uri", { uri: uri, title: title });

  let next_uri = await invoke("get_next_uri");
  window.location = next_uri;
}

window.addEventListener('DOMContentLoaded', () => {
  const { invoke } = window.__TAURI__.tauri;
  if (/neko-sama/.test(window.location.href)) {
    nekosama();
    return;
  }
  if (/fusevideo/.test(window.location.href)) {
    fusevideo();
    return;
  }
  
  invoke("get_next_uri").then(uri => {
    window.location = uri;
  });
});
