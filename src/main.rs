// Prevents additional console window on Windows in release, DO NOT REMOVE!!
#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")]

use std::{
    collections::HashMap, path::PathBuf, process::Stdio, sync::
        Arc
    
};

use hls_m3u8::tags::VariantStream;
use indicatif::MultiProgress;
use indicatif::ProgressStyle;
use reqwest::Client;
use tauri::{App, RunEvent, State, Url, WindowBuilder, WindowUrl};
use tokio::{
    fs::File,
    io::{AsyncBufReadExt, AsyncWriteExt as _},
    process::Command,
    sync::{mpsc::Sender, Mutex},
};
use tracing::{info, info_span};
use tracing_indicatif::{span_ext::IndicatifSpanExt as _, IndicatifLayer};
use tracing_subscriber::{
    layer::SubscriberExt as _,
    util::SubscriberInitExt as _,
};

#[derive(clap::Parser)]
struct Args {
    #[clap(short, long)]
    artefact_dir: PathBuf,
}

// state of the tauri app
// this is mainly composed of channels used to communicate with tokio tasks running alongside the
// app
#[derive(Debug)]
struct AppState {
    // this is behind a mutex because the tauri runtime could be running on multiple threads
    // so the AppState will be shared between them
    rx: Mutex<tokio::sync::mpsc::Receiver<String>>,
    tx: Sender<(String, String)>,
    metadata: Mutex<HashMap<String, String>>,
}

// get the next uri to download
#[tauri::command]
async fn get_next_uri(state: State<'_, AppState>) -> Result<String, String> {
    let mut lock = state.rx.lock().await;
    let ret = lock.recv().await.ok_or("No more uris".to_string());
    ret
}

// send the m3u8 uri to the download task
#[tauri::command]
async fn send_m3u8_uri(
    uri: String,
    title: String,
    state: State<'_, AppState>,
) -> Result<(), String> {
    state.tx.send((uri, title)).await.unwrap();
    info!("Sent uri to download task");
    Ok(())
}

#[tauri::command]
async fn set_metadata(
    state: State<'_, AppState>,
    src: String,
    title: String,
) -> Result<(), String> {
    info!("Setting metadata for {} to {}", src, title);
    let mut metadata = state.metadata.lock().await;
    metadata.insert(src, title);
    Ok(())
}

#[tauri::command]
async fn get_metadata(state: State<'_, AppState>, src: String) -> Result<String, String> {
    info!("Getting metadata for {}", src);
    let metadata = state.metadata.lock().await;
    metadata
        .get(&src)
        .cloned()
        .ok_or("No metadata found".to_string())
}

fn run_tauri(app: App) {
    // create the webview used to navigate through neko-sama
    let window = WindowBuilder::new(
        &app,
        "lol",
        WindowUrl::External(Url::parse("https://example.com").unwrap()),
    )
    .visible(false)
    .initialization_script(include_str!("init.js"))
    .build()
    .unwrap();

    // run the app preventing the exit
    app.run(|_app, evt| {
        if let RunEvent::ExitRequested { api, .. } = evt {
            api.prevent_exit();
        }
    });
}

fn main() {
    let args: Args = clap::Parser::parse();
    let indicatif_layer = IndicatifLayer::new();
    tracing_subscriber::registry()
        .with(tracing_subscriber::fmt::layer().with_writer(indicatif_layer.get_stderr_writer()))
        .with(indicatif_layer)
        .init();
    let mp = Arc::new(MultiProgress::new());

    let (uri_tx, uri_rx) = tauri::async_runtime::channel(3);
    let (tx, mut rx) = tokio::sync::mpsc::channel(128);
    let metadata = Mutex::new(HashMap::new());
    let app = tauri::Builder::default()
        .manage(AppState {
            rx: Mutex::new(uri_rx),
            tx,
            metadata,
        })
        .invoke_handler(tauri::generate_handler![
            get_next_uri,
            send_m3u8_uri,
            set_metadata,
            get_metadata
        ])
        .build(tauri::generate_context!())
        .expect("error while running tauri application");

    // task that handles downloads
    let app_handle = app.handle();

    let tauri_handle = tauri::async_runtime::handle();
    tauri_handle.spawn(async move {
        let mut set = tokio::task::JoinSet::new();
        let mut current_ep = 0;
        loop {
            match rx.recv().await {
                Some((uri, title)) => {
                    info!("Received uri: {}", uri);
                    let uri: String = uri;
                    if set.len() > 10 {
                        set.join_next().await;
                    }

                    let ep = current_ep;
                    current_ep += 1;
                    let dir = args.artefact_dir.clone();
                    set.spawn(async move {
                        let span = info_span!("Download task", ep = ep);
                        let _enter = span.enter();
                        span.pb_set_style(
                            &ProgressStyle::with_template("{prefix} {wide_bar} {pos}/{len} {msg}").unwrap()
                            );
                        // download the master playlist
                        span.pb_set_message("Downloading master playlist");
                        let resp = match Client::new()
                            .get(uri)
                            .header("Accept", "application/x-mpegURL")
                            .header("Accept-Language", "en-US,en;q=0.9")
                            .send()
                            .await
                        {
                            Ok(resp) => resp,
                            Err(e) => {
                                return;
                            }
                        };
                        //
                        // parse master playlist
                        let content = resp.text().await.expect("Failed to get m3u8");
                        let playlist = hls_m3u8::MasterPlaylist::try_from(&*content)
                            .expect("Failed to parse m3u8");

                        // select a variant
                        for variant in playlist.variant_streams {
                            // if the variant is not a stream, skip it
                            let VariantStream::ExtXStreamInf { uri, .. } = variant else {
                                continue;
                            };

                            // try downloading the variant
                            span.pb_set_message("Downloading variant playlist");
                            match Client::new()
                                .get(&*uri)
                                .header("Accept", "application/x-mpegURL")
                                .header("Accept-Language", "en-US,en;q=0.9")
                                .send()
                                .await
                            {
                                // if the request was successful, try to parse the m3u8
                                Ok(resp) if resp.status().is_success() => {
                                    // parse the m3u8 and save it to a tmp file
                                    let content = resp.text().await.expect("Failed to get m3u8");
                                    let playlist = hls_m3u8::MediaPlaylist::try_from(&*content)
                                        .expect("Failed to parse m3u8");
                                    let size = playlist.segments.iter().count();
                                    span.pb_set_length(size as u64);

                                    let nekodl_path = format!("/tmp/nekodl{}.m3u8", ep);
                                    let mut file = File::create(&nekodl_path)
                                        .await
                                        .expect("Failed to create file");
                                    file.write_all(content.as_bytes())
                                        .await
                                        .expect("Failed to write file");
                                    drop(file);

                                    // download the stream using ffmpeg to
                                    // /home/lily/Vidéos/fuck.mp4
                                    span.pb_set_message("Downloading media");
                                    let filepath = dir.join(format!("{}.mp4", title));
                                    let mut child = Command::new("ffmpeg")
                                        .args(&[
                                            "-y",
                                            "-protocol_whitelist",
                                            "file,http,https,tcp,tls",
                                            "-i",
                                            &nekodl_path,
                                            "-c",
                                            "copy",
                                            &filepath.to_string_lossy(),
                                        ])
                                        .stdout(Stdio::null())
                                        .stderr(Stdio::piped())
                                        .spawn()
                                        .unwrap();

                                    // read lines from ffmpeg's stderr, and write only lines
                                    // containing the string ".ts" to show the ""progress"" of
                                    // ffmpeg
                                    let s = tokio::io::BufReader::new(child.stderr.take().unwrap());
                                    let mut lines = s.lines();
                                    while let Ok(Some(l)) = lines.next_line().await {
                                        if l.contains(".ts") {
                                            span.pb_inc(1);
                                        }
                                    }

                                    if child.wait().await.unwrap().success() {
                                    } else {
                                    }

                                    return;
                                }
                                Ok(resp) => {
                                    return;
                                }
                                Err(e) => {
                                    return;
                                }
                            };
                        }
                    });
                }
                None => break,
            }
        }

        while !set.is_empty() {
            set.join_next().await;
        }

        app_handle.exit(0);
    });

    // test with some payload
    let payload = "https://neko-sama.fr/anime/episode/8119-owarimonogatari-01_vostfr";
    uri_tx.blocking_send(payload.to_owned()).unwrap();
    let payload = "https://neko-sama.fr/anime/episode/8119-owarimonogatari-02_vostfr";
    uri_tx.blocking_send(payload.to_owned()).unwrap();

    run_tauri(app);
}
